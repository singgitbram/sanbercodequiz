import React, { useContext } from 'react';
import { Text, View, FlatList } from 'react-native';
import { RootContext } from '../QUIZ1'

export default function Soal2() {

    const state = useContext(RootContext)

    const renderan = ({ item }) => {
        return (
            <View>
                <Text>{item.name}</Text>
                <Text>{item.position}</Text>
            </View>
        )
    }

    return (
        <View>
            <FlatList
                data={state.name}
                renderItem={renderan}
                keyExtractor={(key, index) => index.toString()}
            />
        </View>
    );
}
